from dataclasses import dataclass
import typing
import requests
import re
import json
import math


@dataclass
class WowheadObject:
    type: str
    name: str
    ids: typing.List[str]
    coordinates: dict
    gathermate_id: str
    url_part: str = "object"

    def __init__(self, type: str, name: str, ids: typing.List[str], gathermate_id: str):
        self.type = type
        self.name = name
        self.ids = ids
        self.coordinates = dict()
        self.gathermate_id = gathermate_id

        for object_id in self.ids:
            if self.type == "classic":
                url = "https://wowhead.com/wotlk"
            elif self.type == "era":
                url = "https://classic.wowhead.com"
            else:
                raise "Invalid type"
            result = requests.get(f'{url}/{self.url_part}={object_id}')
            data = re.search(r'var g_mapperData = (.*);', result.text)
            try:
                data_parsed = json.loads(data.group(1))
            except AttributeError:
                print(f"No locations for {object_id} ({self.name})")
                continue
            for zone in data_parsed:
                wow_zone = WOWHEAD_ZONE_MAP.get(zone)
                if wow_zone is None:
                    print(f"Found unlisted zone: {zone}")
                    continue
                coords = list()
                for coord in data_parsed[zone][0]["coords"]:
                    coords.append(Coordinate(coord[0], coord[1]))
                if self.coordinates.get(wow_zone) is None:
                    self.coordinates[wow_zone] = coords
                else:
                    self.coordinates[wow_zone] += coords
        print(f"Finished processing {self.name}")


class WowheadNpc(WowheadObject):
    url_part: str = "npc"


@dataclass(eq=True, frozen=True)
class Zone:
    name: str
    id: str


@dataclass
class Coordinate:
    x: float
    y: float
    coord: int = 0

    def __repr__(self):
        return str(self.as_gatherer_coord())

    def as_gatherer_coord(self):
        if self.coord == 0:
          self.coord = math.floor((self.x/100)*10000+0.5)*1000000+math.floor((self.y/100)*10000+0.5)*100
        return self.coord


@dataclass
class GathererEntry:
    coordinate: Coordinate
    entry_id: str

    def __repr__(self):
        return f"		[{self.coordinate}] = {self.entry_id},"

    def __lt__(self, other):
        return self.coordinate.as_gatherer_coord() < other.coordinate.as_gatherer_coord()


@dataclass
class GathererZone:
    zone: Zone
    entries: typing.List[GathererEntry]

    def __repr__(self):
        output = f'	[{self.zone.id}] = {{\n'
        for entry in sorted(self.entries):
            output += f'{str(entry)}\n'
        output += '	},\n'
        return output

    def __lt__(self, other):
        return int(self.zone.id) < int(other.zone.id)


@dataclass
class Aggregate:
    type: str
    zones: typing.List[GathererZone]

    def __init__(self, type, objects):
        self.type = type
        self.zones = []
        for object in objects:
            for zone in object.coordinates:
                for coord in object.coordinates[zone]:
                    self.add(zone, GathererEntry(coord, object.gathermate_id))

    def __repr__(self):
        output = f"GatherMateData2{self.type}DB = {{\n"
        for zone in sorted(self.zones):
            output += f'{str(zone)}'
        output += '}'
        return output

    def add(self, zone: Zone, entry: GathererEntry):
        for gatherer_zone in self.zones:
            if gatherer_zone.zone == zone:
                while entry.coordinate in [x.coordinate for x in gatherer_zone.entries]:
                  entry.coordinate.coord = entry.coordinate.as_gatherer_coord() + 1
                gatherer_zone.entries.append(entry)
                return
        self.zones.append(GathererZone(zone, [entry]))


# key is zone id as it appears in wowhead
# GathererEntry is the id from `/dump C_Map.GetBestMapForUnit("player");` while in the zone
WOWHEAD_ZONE_MAP = {
    '14': Zone("Durotar", "1411"),
    '215': Zone("Mulgore", "1412"),
    '17': Zone("The Barrens", "1413"),
    '36': Zone("Alterac Mountains", "1416"),
    '45': Zone("Arathi Highlands", "1417"),
    '3': Zone("Badlands", "1418"),
    '4': Zone("Blasted Lands", "1419"),
    '85': Zone("Tirisfal Glades", "1420"),
    '130': Zone("Silverpine Forest", "1421"),
    '28': Zone("Western Plaguelands", "1422"),
    '139': Zone("Eastern Plaguelands", "1423"),
    '267': Zone("Hillsbrad Foothills", "1424"),
    '47': Zone("The Hinterlands", "1425"),
    '1': Zone("Dun Morogh", "1426"),
    '51': Zone("Searing Gorge", "1427"),
    '46': Zone("Burning Steppes", "1428"),
    '12': Zone("Elwynn Forest", "1429"),
    '41': Zone("Deadwind Pass", "1430"),
    '10': Zone("Duskwood", "1431"),
    '38': Zone("Loch Modan", "1432"),
    '44': Zone("Redridge Mountains", "1433"),
    '33': Zone("Stranglethorn Vale", "1434"),
    '8': Zone("Swamp of Sorrows", "1435"),
    '40': Zone("Westfall", "1436"),
    '11': Zone("Wetlands", "1437"),
    '141': Zone("Teldrassil", "1438"),
    '148': Zone("Darkshore", "1439"),
    '331': Zone("Ashenvale", "1440"),
    '400': Zone("Thousand Needles", "1441"),
    '406': Zone("Stonetalon Mountains", "1442"),
    '405': Zone("Desolace", "1443"),
    '357': Zone("Feralas", "1444"),
    '15': Zone("Dustwallow Marsh", "1445"),
    '440': Zone("Tanaris", "1446"),
    '16': Zone("Azshara", "1447"),
    '361': Zone("Felwood", "1448"),
    '490': Zone("Un'Goro Crater", "1449"),
    '493': Zone("Moonglade", "1450"),
    '1377': Zone("Silithus", "1451"),
    '618': Zone("Winterspring", "1452"),
    '3430': Zone("Eversong Woods", "1941"),
    '3433': Zone("Ghostlands", "1942"),
    '3524': Zone("Azuremyst Isle", "1943"),
    '3525': Zone("Bloodmyst Isle", "1950"),
    '3483': Zone("Hellfire Peninsula", "1944"),
    '3518': Zone("Nagrand", "1951"),
    '3519': Zone("Terokkar Forest", "1952"),
    '3520': Zone("Shadowmoon Valley", "1948"),
    '3521': Zone("Zangarmarsh", "1946"),
    '3522': Zone("Blade's Edge Mountains", "1949"),
    '3523': Zone("Netherstorm", "1953"),
    '4080': Zone("Isle of Quel'Danas", "1957"),
    '3537': Zone("Borean Tundra", "114"),
    '4197': Zone("Wintergrasp", "123"),
    '495': Zone("Howling Fjord", "117"),
    '67': Zone("The Storm Peaks", "120"),
    '65': Zone("Dragonblight", "115"),
    '66': Zone("Zul'Drak", "121"),
    '210': Zone("Icecrown", "118"),
    '3711': Zone("Sholazar Basin", "119"),
    '394': Zone("Grizzly Hills", "116"),
    '2817': Zone("Crystalsong Forest", "127"),
    '4742': Zone("Hrothgar's Landing", "170"),
    # '1638': Zone("Thunder Bluff", ""),
    # '3703': Zone("Shattrath City", ""),
    '4709': Zone("Southern Barrens", "199"),
    '5034': Zone("Uldum", "249"),
    '5695': Zone("Ahn'Qiraj: The Fallen Kingdom", "327"),
    '616': Zone("Mount Hyjal", "198"),
    '5042': Zone("Deepholm", "207"),
    '4922': Zone("Twilight Highlands", "241"),
    '5095': Zone("Tol Barad", "244"),
    '5146': Zone("Vashj'ir", "203"),
    '5144': Zone("Shimmering Expanse", "205"),
    '5861': Zone("Darkmoon Island", "408"),
    '5145': Zone("Abyssal Depths", "204"),
    '4706': Zone("Ruins of Gilneas", "217"),
    '5733': Zone("Molten Front", "338"),
    '4815': Zone("Kelp'thar Forest", "201"),
    # '5339': Zone("Stranglethorn Vale", ""),
    '5287': Zone("The Cape of Stranglethorn", "210"),
    '5389': Zone("Tol Barad Peninsula", "245"),
    '4720': Zone("The Lost Isles", "174"),
}
